export const removePay = function (payments){
    const removeEl = document.querySelectorAll('.form__summary-list .single_list__item');
    const totalEl = document.querySelector('.list__total .price b');

    removeElement(removeEl);
    payments.length = 0;
    totalEl.innerHTML = 0;

};

function removeElement (obj) {
    obj.forEach(el => el.remove());
}