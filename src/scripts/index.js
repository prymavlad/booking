import '../styles/index.scss';

import { tarifs } from './constants';
import { paymentObjResult } from './validations';
import { createPay } from './create-pay';
import { removePay } from './remove-pay';
import { savePay, selectedPay } from './payments';

let balance = 100;
const payments = [];
const payment = {};

const servicesList = document.querySelectorAll('.left__company');
const generalTitle = document.querySelector('.center__title');
const generalDescription = document.querySelector('.center__desc');
const meterId = document.querySelector('#meters');
const previous = document.querySelector('#previous');
const current = document.querySelector('#current');
const submitBtn = document.querySelector('#save');
const resetBtn = document.querySelector('#reset');
const payBtn = document.querySelector('#pay');
submitBtn.setAttribute('disabled', 'disabled');

servicesList.forEach(el => {

    el.addEventListener('click', (event) => {

        for(let i = 0; i < servicesList.length; i++) {
            servicesList[i].classList.remove('selected');
        }
        el.classList.add('selected');

        const id = el.getAttribute('data-id');
        const name = el.querySelector('.left__company-desc').textContent;
        let description = '';

        if(id === 'taxes') {
            description = 'Оплата налогов';
        } else if(id === 'water') {
            description = 'Оплата холодного водоснабжения';
        } else if(id === 'internet') {
            description = 'Оплата интернета';
        } else if(id === 'security') {
            description = 'Оплата охраны';
        } else if(id === 'exchange') {
            description = 'Оплата обмена валют';
        }

        payment.id = id;

        generalTitle.innerHTML = name;
        generalDescription.innerHTML = description;

        paymentObjResult(payment, meterId, previous, current);
    });

});

meterId.addEventListener('change', () => paymentObjResult(payment, meterId, previous, current));
previous.addEventListener('input', () => paymentObjResult(payment, meterId, previous, current));
current.addEventListener('input', () => paymentObjResult(payment, meterId, previous, current));

submitBtn.addEventListener('click', function (e){
    e.preventDefault();

    const tarif = tarifs[payment.id];
    const meter = payment.meterId;
    const id = payment.id;
    const difference = ((payment.current * 10000) - (payment.previous * 10000)) / 10000;
    const toPay = (tarif * difference).toFixed(2);

    payments.push({
        id: id,
        meter: meter,
        toPay: toPay
    });

    savePay(payments);

    payment.id = null;
    payment.meterId = null;
    payment.previous = null;
    payment.current = null;
    current.value = '';
    previous.value = '';
    submitBtn.setAttribute('disabled', 'disabled');

    createPay(payments);

});

resetBtn.addEventListener('click', () => removePay(payments));

payBtn.addEventListener('click', function (e) {
   e.preventDefault();

    selectedPay(payments, balance);

});

console.log(tarifs);