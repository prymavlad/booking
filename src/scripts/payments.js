export const savePay = function (payments){
    const savePayContainer = document.querySelector('.right__payments-fields');
    const supportArr = [];

    savePayContainer.innerHTML = '';

    payments.forEach((el) => {

        if(!(supportArr.includes(el.id))) {
            supportArr.push(el.id);
        } else {
            return;
        }

        const savePayEl = document.createElement('p');
        savePayEl.classList.add('right__payments-field');
        const label = document.createElement('label');
        const input = document.createElement('input');
        input.setAttribute('type', 'checkbox');
        input.setAttribute('id', el.id);
        const span = document.createElement('span');

        label.append(input);
        label.append(span);
        savePayEl.append(label);

        if(el.id === 'taxes'){
            span.innerHTML = 'Налоги';
        } else if(el.id === 'water') {
            span.innerHTML = 'Холодная вода';
        } else if(el.id === 'internet') {
            span.innerHTML = 'Интернет';
        } else if(el.id === 'security') {
            span.innerHTML = 'Охрана';
        } else if(el.id === 'exchange') {
            span.innerHTML = 'Обмен валют';
        }

        savePayContainer.append(savePayEl);

    });

};

export const selectedPay = function (payments,balance){

    const selectedServices = document.querySelectorAll('.right__payments-field input:checked');
    const transactionsContainer = document.querySelector('.transactions__list');
    transactionsContainer.innerHTML = '';
    let transactionErr = false;

    payments.forEach((el) => {

        selectedServices.forEach((elem) => {
            const transactionItem = document.createElement('li');
            transactionItem.classList.add('list__item');

            if(el.id === elem.id  && transactionErr === false) {

                console.log(balance, 'balance');
                console.log(el.toPay, 'el');

                if((balance - el.toPay) >= 0) {

                    transactionItem.classList.remove('list__item-error');
                    console.log(`${elem.nextSibling.textContent}: успешно оплачено`);
                    transactionItem.innerHTML = `${elem.nextSibling.textContent}: успешно оплачено`;

                    balance = balance - el.toPay;

                } else {

                    transactionItem.classList.add('list__item-error');
                    console.log(`${elem.nextSibling.textContent}: ошибка транзакции`);
                    transactionItem.innerHTML = `${elem.nextSibling.textContent}: ошибка транзакции`;
                    transactionErr = true;

                }
                transactionsContainer.append(transactionItem);

            } else {

                return;

            }

        });

    });



};