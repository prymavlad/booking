export const createPay = function (payments){
    const payContainer = document.querySelector('.form__summary-list');
    const removeEl = document.querySelectorAll('.form__summary-list .single_list__item');
    const totalEl = document.querySelector('.list__total .price b');

    removeElement(removeEl);

    payments.forEach((obj) => {

        payContainer.prepend(createElement(obj));

    });

    const allPayments = document.querySelectorAll('.single_list__item .price b');
    const allPaymentsArr = [];
    allPayments.forEach((el) => allPaymentsArr.push(el.innerText));

    const total = allPaymentsArr.reduce(function (previousValue, currentValue) {
        return previousValue + Number(currentValue);
    }, 0);

    totalEl.innerHTML = total.toFixed(2);

};

function createElement(obj) {

    const singleItem = document.createElement('li');
    const singleItemWrapper = document.createElement('p');
    const price = document.createElement('span');
    const label = document.createElement('span');
    const payValue = document.createElement('b');

    singleItem.setAttribute('class', 'list__item single_list__item');
    price.setAttribute('class', 'price');
    label.setAttribute('class', 'list__item-label');
    price.innerHTML = `$ `;
    payValue.innerHTML = `${obj.toPay}`;
    label.innerHTML = `${obj.meter}`;

    singleItemWrapper.append(label);
    label.append();
    singleItemWrapper.append(price);
    price.append(payValue);
    singleItem.append(singleItemWrapper);

    return singleItem;

}

function removeElement (obj) {
    obj.forEach(el => el.remove());
}