export const paymentObjResult = function (payment, meterId, previous, current) {
    const submitBtn = document.querySelector('#save');

    const prevValue = validation(previous);
    const currValue = validation(current);

    if(prevValue !== false && currValue !== false && currValue > prevValue) {

        payment.meterId = meterId.options[meterId.selectedIndex].text;
        payment.previous = prevValue;
        payment.current = currValue;
        submitBtn.removeAttribute('disabled');

    } else {

        current.classList.add('error');
        submitBtn.setAttribute('disabled', 'disabled');

    }

};

function validation (inputEl) {

    const value = inputEl.value;

    if(Number(value) === 0 || isNaN(Number(value)) || (Math.sign(value) !== 1 && Math.sign(value) !== 0)) {

        inputEl.classList.add('error');
        return false;

    }

    inputEl.classList.remove('error');

    return Number(value);

};